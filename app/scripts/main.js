function hideAllError(str) { return true; }
window.onerror = hideAllError;

$(document).ready(function () {

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  $('.s-header__toggle').on('click', function (e) {
    e.preventDefault();

    $('.s-header .col-xl-10').slideToggle('fast');
  });

  $('.s-area-dot__number').on('click', function (e) {
    e.preventDefault();

    $('.s-area-dot__content').hide();
    $(this).next().toggle();
  })

  $('.open-rooms').on('click', function (e) {
    e.preventDefault();

    $('.s-rooms-modal').toggle();
    rgalleryThumbs.update();
    rgalleryTop.update();
  });

  $('.s-rooms-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 's-rooms-modal__centered') {
      $('.s-rooms-modal').hide();
    }
  });

  $('.s-rooms-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.s-rooms-modal').hide();
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.s-modal').toggle();
  });

  $('.s-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 's-modal__centered') {
      $('.s-modal').hide();
    }
  });

  $('.s-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.s-modal').hide();
  });

  new Swiper('.s-stock__cards', {
    pagination: {
      el: '.s-stock__centered .swiper-pagination',
      clickable: 'true',
    },
    navigation: {
      nextEl: '.s-stock__centered .swiper-button-next',
      prevEl: '.s-stock__centered .swiper-button-prev',
    },
    spaceBetween: 30,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    }
  });

  new Swiper('.s-specs__cards', {
    pagination: {
      el: '.s-specs__centered .swiper-pagination',
      clickable: 'true',
    },
    navigation: {
      nextEl: '.s-specs__centered .swiper-button-next',
      prevEl: '.s-specs__centered .swiper-button-prev',
    },
    spaceBetween: 50,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('.s-rest__cards', {
    pagination: {
      el: '.s-rest__centered .swiper-pagination',
      clickable: 'true',
    },
    navigation: {
      nextEl: '.s-rest__centered .swiper-button-next',
      prevEl: '.s-rest__centered .swiper-button-prev',
    },
    spaceBetween: 30,
    slidesPerView: 1,
    slidesPerColumn: 2,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    }
  });

  new Swiper('.s-home-reviews__cards', {
    pagination: {
      el: '.s-home-reviews .swiper-pagination',
      clickable: 'true',
    },
    navigation: {
      nextEl: '.s-home-reviews .swiper-button-next',
      prevEl: '.s-home-reviews .swiper-button-prev',
    },
    breakpoints: {
      1200: {
        slidesPerView: 2,
      },
    }
  });

  $('.s-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.s-tabs__header li').removeClass('current');
    $('.s-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');

    ngalleryThumbs.update();
    ngalleryTop.update();
  });

  $(window).on('load resize scroll', function (e) {

    if ($(window).width() > 1199) {

      /* $('#video').YTPlayer({
        fitToBackground: true,
        videoId: 'N8Or8_v_bNs',
        pauseOnScroll: false,
        playerVars: {
          modestbranding: 0,
          autoplay: 1,
          controls: 0,
          showinfo: 0,
          branding: 0,
          rel: 0,
          autohide: 0,
          start: 5
        }
      }); */
    }
  });

  $('#rating').barrating({
    theme: 'fontawesome-stars-o',
    showSelectedRating: false
  });

  new Swiper('.s-video__cards', {
    navigation: {
      nextEl: '.s-video .swiper-button-next',
      prevEl: '.s-video .swiper-button-prev',
    },
    spaceBetween: 55,
    loop: true,
  });

  new Swiper('.s-photos__cards', {
    slidesPerView: 1,
    slidesPerColumn: 2,
    spaceBetween: 30,
    navigation: {
      nextEl: '.s-photos .swiper-button-next',
      prevEl: '.s-photos .swiper-button-prev',
    },
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    }
  });

  new Swiper('.s-wins__cards', {
    spaceBetween: 30,
    navigation: {
      nextEl: '.s-wins .swiper-button-next',
      prevEl: '.s-wins .swiper-button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
    }
  });

  var sgalleryThumbs = new Swiper('.s-therapy__small', {
    spaceBetween: 30,
    slidesPerView: 1,
    freeMode: false,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
      768: {
        slidesPerView: 2,
        freeMode: true,
      },
      1024: {
        slidesPerView: 3,
        freeMode: true,
      },
      1200: {
        slidesPerView: 4,
        freeMode: true,
      },
    }
  });
  var sgalleryTop = new Swiper('.s-therapy__big', {
    spaceBetween: 60,
    navigation: {
      nextEl: '.s-therapy__small .swiper-button-next',
      prevEl: '.s-therapy__small .swiper-button-prev',
    },
    thumbs: {
      swiper: sgalleryThumbs
    }
  });

  var rgalleryThumbs = new Swiper('.s-rooms-modal__small', {
    spaceBetween: 0,
    slidesPerView: 8,
    freeMode: true,
    direction: 'vertical',
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var rgalleryTop = new Swiper('.s-rooms-modal__big', {
    spaceBetween: 0,
    thumbs: {
      swiper: rgalleryThumbs
    }
  });

  var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 'auto',
    freeMode: false,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 50,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    thumbs: {
      swiper: galleryThumbs
    }
  });

  var ngalleryThumbs = new Swiper('.ngallery-thumbs', {
    spaceBetween: 50,
    slidesPerView: 'auto',
    freeMode: false,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var ngalleryTop = new Swiper('.ngallery-top', {
    spaceBetween: 10,
    thumbs: {
      swiper: ngalleryThumbs
    }
  });
});
